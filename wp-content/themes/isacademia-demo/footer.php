<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ISAcademia
 */

?>

			<!-- #Page content -->
			</div>
		</div>
	</div>

	<footer id="colophon"" role="contentinfo">
		<div class="row">
			<div class="columns">
				<div class="site-footer">
					<h1>Ecole IS-Academia</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="columns footer-bloc">
				<div class="site-footer">
					<p>Un site web public propulsé par Wordpress, développé par Equinoxe MIS Development</p>
				</div>
			</div>
			<div class="columns footer-bloc">
				<div class="site-footer">
					<p>Autres sites :</p>
					<a href="<?php echo esc_url('http://wwww.epfl.ch'); ?>" target="_blank">EPFL</a><br/>
					<a href="<?php echo esc_url('http://wwww.equinoxemis.ch'); ?>" target="_blank">Equinoxe MIS Development</a>
				</div>
			</div>
			<div class="columns footer-bloc">
				<div class="site-footer">
					<p>Pour nous contacter :</p>
					<a href="<?php echo esc_url('mailto:info@equinoxemis.ch'); ?>">info[a]equinoxemis.ch</a>
				</div>
			</div>
			<div class="columns footer-bloc">
				<div class="site-footer">
					<p>&copy; Equinoxe MIS Development | 2017</p>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<script src="<?php echo get_theme_file_uri().'/foundation/jquery.js'; ?>"></script>
<script src="<?php echo get_theme_file_uri().'/foundation/what-input.js'; ?>"></script>
<script src="<?php echo get_theme_file_uri().'/foundation/foundation.min.js'; ?>"></script>
<script src="<?php echo get_theme_file_uri().'/js/app.js'; ?>"></script>

<?php wp_footer(); ?>

</body>
</html>
