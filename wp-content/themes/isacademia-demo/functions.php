<?php
/**
 * Created by PhpStorm.
 * User: jclerc
 * Date: 06.04.17
 * Time: 11:28
 */

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
        'name' => 'btnCell-ecole',
        'id'   => 'btnCell-ecole'
    ));
    register_sidebar(array(
        'name' => 'btnCell-metiers',
        'id'   => 'btnCell-metiers'
    ));
    register_sidebar(array(
        'name' => 'btnCell-formations',
        'id'   => 'btnCell-formations'
    ));
    register_sidebar(array(
        'name' => 'btnCell-boutique',
        'id'   => 'btnCell-boutique'
    ));
    register_sidebar(array(
        'name' => 'btnCell-actus',
        'id'   => 'btnCell-actus'
    ));
    register_sidebar(array(
        'name' => 'btnCell-contact',
        'id'   => 'btnCell-contact'
    ));
    register_sidebar(array(
        'name' => 'imgCell1',
        'id'   => 'imgCell1'
    ));
    register_sidebar(array(
        'name' => 'imgCell2',
        'id'   => 'imgCell2'
    ));
    register_sidebar(array(
        'name' => 'lst-etu1',
        'id'   => 'lst-etu1'
    ));
    register_sidebar(array(
        'name' => 'lst-etu2',
        'id'   => 'lst-etu2'
    ));
    register_sidebar(array(
        'name' => 'context-side-menu',
        'id'   => 'context-side-menu'
    ));
    register_sidebar(array(
        'name' => 'lst-formations',
        'id'   => 'lst-formations'
    ));
}