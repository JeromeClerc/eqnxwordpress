<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ISAcademia
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" href="<?php echo get_theme_file_uri().'/foundation/foundation.min.css'; ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'isacademia' ); ?></a>


		<div class="off-canvas-wrapper">
			<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
				<div class="off-canvas position-left" id="offCanvas" data-off-canvas>
					<!-- Close button -->
					<button class="close-button" aria-label="Close menu" type="button" data-close>
						<span aria-hidden="true">&times;</span>
					</button>
					<!-- Menu -->
					<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-mobile-menu', 'menu_class' => 'vertical menu' ) ); ?>
				</div>

				<!-- Title bar -->
				<div class="title-bar show-for-small-only">
					<div class="title-bar-left">
						<button class="menu-icon" type="button" data-toggle="offCanvas"></button>
					</div>
					<div class="title-bar-right">
						<span class="title-bar-title"><?php bloginfo( 'name' ); ?></span>
					</div>
				</div>
				<!-- #Title bar -->

				<div class="off-canvas-content" data-off-canvas-content>
					<!-- header -->
					<header id="masthead" class="site-header hide-for-small-only" role="banner">
						<div class="row">
							<div class="small-6 medium-3 columns">
								<!-- #site-branding -->
								<div class="site-branding">
									<?php
									if ( is_front_page() && is_home() ) : ?>
										<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
											<img src="<?php echo get_bloginfo('template_url') ?>/images/logo-l.png"/>
										</a>
									<?php else : ?>
										<p class="site-title">
											<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
												<img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/logo-l.png"/>
											</a>
										</p>
									<?php endif;  ?>
								</div>
								<!-- #site-branding -->
							</div>
							<div class="medium-9 columns">
								<nav id="site-navigation" class="main-navigation" role="navigation">
									<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
								</nav><!-- #site-navigation -->
							</div>
						</div>
					</header>
					<!-- #header -->
					<!-- Page content -->
					<div id="content" class="site-content">
