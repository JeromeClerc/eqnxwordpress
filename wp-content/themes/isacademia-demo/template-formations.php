<?php
/*
Template Name: Formations
*/
get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="row page-header">
                <div class="small-12 medium-4 columns page-header-title orange">
                    <h1>Formations<br/><small>continues</small></h1>
                </div>
                <div class="medium-8 columns hide-for-small-only page-header-image">
                    <img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/header-formations.jpg"/>
                </div>
            </div>
            <div class="row page-body">
                <div class="medium-4 columns page-body-menu">
                    <?php if ( !dynamic_sidebar('context-side-menu') ) ?>
                </div>
                <div class="small-12 medium-8 columns page-body-content" id="page-body-content">
                    <div class="row">
                        <div class="small-12 columns">
                            <h1>Formations continues</h1>
                            <h2>Introduction</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sed sapien quam. Sed dapibus est id enim facilisis, at posuere turpis adipiscing. Quisque sit amet dui dui.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns" id="lst-formations">
                            <?php if ( !dynamic_sidebar('lst-formations') ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php

get_footer();
