<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ISAcademia
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="row">
				<div class="small-12 medium-6 columns btn-cell-slot">
					<?php if ( !dynamic_sidebar('btnCell-ecole') ) ?>
				</div>
				<div class="small-12 medium-6 columns">
					<h2>lauréats 2016</h2>
					<div class="row">
						<div class="small-12 columns">
							<?php if ( !dynamic_sidebar('lst-etu1') ) ?>
						</div>
						<div class="small-12 columns">
							<?php if ( !dynamic_sidebar('lst-etu2') ) ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row"  data-equalizer data-equalize-on="large">
				<div class="small-12 medium-6 large-5 columns" data-equalizer-watch>
					<?php if ( !dynamic_sidebar('btnCell-metiers') ) ?>
				</div>
				<div class="small-12 medium-6 large-5 columns" data-equalizer-watch>
					<?php if ( !dynamic_sidebar('btnCell-formations') ) ?>
				</div>
				<div class="large-2 columns hide-for-medium-only hide-for-small-only" data-equalizer-watch>
					<?php if ( !dynamic_sidebar('imgCell1') ) ?>
				</div>
			</div>

			<div class="row">
				<div class="small-12 medium-6 large-6 columns">
					<div class="row">
						<div class="large-6 columns hide-for-medium-only hide-for-small-only">
							<?php if ( !dynamic_sidebar('imgCell2') ) ?>
						</div>
						<div class="small-12 medium-12 large-6 columns">
							<?php if ( !dynamic_sidebar('btnCell-boutique') ) ?>
						</div>
					</div>
					<div class="row">
						<div class="columns">
							Eduquoa
						</div>
					</div>
				</div>

				<div class="small-12 medium-6 large-6 columns">
					<?php if ( !dynamic_sidebar('btnCell-actus') ) ?>
				</div>
			</div>

			<div class="row">
				<div class="columns">
					<?php if ( !dynamic_sidebar('btnCell-contact') ) ?>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
