<?php
/*
Template Name: Ecole
*/
get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="row page-header">
                <div class="small-12 medium-4 columns page-header-title green-mint">
                    <h1>L'école<br/><small>de la construction</small></h1>
                </div>
                <div class="medium-8 columns hide-for-small-only page-header-image">
                    <img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/header-ecole.jpg"/>
                </div>
            </div>
            <div class="row page-body">
                <div class="medium-4 columns page-body-menu">
                    NAV
                </div>
                <div class="small-12 medium-8 columns page-body-content">
                    <?php
                        $content = query_posts('category_name=ecole');
                        echo $content[0]->post_content;
                    ?>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php

get_footer();
