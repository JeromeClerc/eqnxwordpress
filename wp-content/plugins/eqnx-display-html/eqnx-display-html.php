<?php
/*
Plugin Name: Eqnx - Display HTML
Plugin URI: http://equinoxemis.ch
Description: Widget Displays HTML - Affiche les informations reçuent de fichiers HTML externes
Version: 1.4
Author: Jérôme Clerc
Author URI: http://equinoxemis.ch
License: GPL2
*/

class displayHtmlWidget extends WP_Widget {
    private $_edg;

    public function __construct() {
        $widget_ops = array('classname' => 'displayHtmlWidget', 'description' => 'Affiche les informations reçuent de fichiers HTML externes' );
        parent::__construct('displayHtmlWidget', 'Eqnx - Display HTML', $widget_ops);
        $this->_edg = $GLOBALS['eqnx_edg'];
    }

    public function get_widget_id() {
        return $this->_widget_id;
    }

    public function set_widget_id($id) {
        $this->_widget_id = $id;
    }

    public function form($instance) {
        $instance = wp_parse_args((array) $instance, array( 'widget_title' => '', 'file_url_root' => '', 'file_url_add' => '', 'api_key' => '', 'data_validity' => '', 'empty_cache' => '' ));
        $widgetTitle = $instance['widget_title'];
        $fileUrlRoot = $instance['file_url_root'];
        $fileUrlAdd = $instance['file_url_add'];
        $apiKey = $instance['api_key'];
        $dataValidity = $instance['data_validity'];

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('widget_title'); ?>">Titre du widget: (optionnel) <input class="widefat" id="<?php echo $this->get_field_id('widget_title'); ?>" name="<?php echo $this->get_field_name('widget_title'); ?>" type="text" value="<?php echo esc_attr($widgetTitle); ?>" /></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('file_url_root'); ?>">URL racine: (obligatoire) <input class="widefat" id="<?php echo $this->get_field_id('file_url_root'); ?>" name="<?php echo $this->get_field_name('file_url_root'); ?>" type="text" value="<?php echo esc_attr($fileUrlRoot); ?>" /></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('file_url_add'); ?>">URL base: (optionnel) <input class="widefat" id="<?php echo $this->get_field_id('file_url_add'); ?>" name="<?php echo $this->get_field_name('file_url_add'); ?>" type="text" value="<?php echo esc_attr($fileUrlAdd); ?>" /></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('api_key'); ?>">API Key: (optionnel) <input class="widefat" id="<?php echo $this->get_field_id('api_key'); ?>" name="<?php echo $this->get_field_name('api_key'); ?>" type="text" value="<?php echo esc_attr($apiKey); ?>" /></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('data_validity'); ?>">
                Validité des données: (par défaut &infin;)
                <select class="widefat" id="<?php echo $this->get_field_id('data_validity'); ?>" name="<?php echo $this->get_field_name('data_validity'); ?>">
                    <option value="<?php echo esc_attr($dataValidity); ?>" selected><?php echo $this->getDelay(esc_attr($dataValidity)); ?></option>
                    <option value="43200">12 heures</option>
                    <option value="86400">1 jours</option>
                    <option value="172800">2 jours</option>
                    <option value="259200">3 jours</option>
                    <option value="345600">4 jours</option>
                    <option value="432000">5 jours</option>
                    <option value="604800">1 semaine</option>
                    <option value="1209600">2 semaine</option>
                    <option value="1814400">3 semaine</option>
                    <option value="2419200">1 mois</option>
                </select>
            </label>
        </p>
        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['widget_title'] = $new_instance['widget_title'];
        $instance['file_url_root'] = $new_instance['file_url_root'];
        $instance['file_url_add'] = $new_instance['file_url_add'];
        $instance['file_url'] = $this->concat_url(array($instance['file_url_root'], $instance['file_url_add']));
        $instance['api_key'] = $new_instance['api_key'];
        $instance['data_validity'] = $new_instance['data_validity'];

        $result = $this->_edg->getFile($instance);
        $instance['req_code'] = wp_remote_retrieve_response_code($result);
        $instance['req_message'] = wp_remote_retrieve_response_message($result);

        return $instance;
    }

    public function widget($args, $instance) {
        extract($args, EXTR_SKIP);
        $this->getWidgetId($this->id);       

        $fileUrl = empty($instance['file_url']) ? '' : $instance['file_url'];
        $instance['data_validity'] = empty($instance['data_validity']) ? 0 : $instance['data_validity'];

        // WIDGET CODE GOES HERE
        if($instance['req_code'] === 200) {
            if ($fileUrl !== '') {
                $this->display_html($instance);
            }
        }
        else {
            echo $this->display_error('Erreur avec le fichier [CODE:'.$instance['req_code'].'] : '.$instance['req_message']);
        }
    }

    private function concat_url($urls) {
        $result = '';
        foreach($urls as $k => $url) {
            $url = $this->remove_first_slash($url);
            $url = $this->remove_last_slash($url);
            $result .= $url;
        }
        return $result;
    }

    private function remove_first_slash($url) {
        if(stripos(substr($url, 0, 1), '/') === 0) {
            return substr($url, 1);
        }
        else {
            return $url;
        }
    }

    private function remove_last_slash($url) {
        if(stripos(substr($url, -1), '/') === 0) {
            return substr($url, 0, -1);
        }
        else {
            return $url;
        }
    }

    private function get_html_from_db($instance) {
        try {
            return $this->_edg->get_content(array('field' => 'url', 'value' => $instance['file_url']));
        }
        catch (Exception $e) {
            throw new Exception($e);
        }
    }

    private function getWidgetId($id) {
        $numId = explode('-', $id);
        $this->set_widget_id($numId[1]);
    }

    private function display_html($instance) {
        $data = $this->get_html_from_db($instance);

        if($instance['data_validity'] > 0 && (time() - intval($data[0]->last_update)) > $instance['data_validity']) {
            $data = $this->load_data($instance);
        }

        $nonce = wp_create_nonce("my_user_vote_nonce");

        $html = $this->build_html($data, $instance);
        $html = $this->update_html($html, $nonce, $data[0]->code);

        echo $html;
    }

    public function get_html_content($instance) {
        $settings = $this->get_settings();

        if($instance['det_url'] !== '') {
            $instance['file_url'] = $this->concat_url(array($settings[$instance['widget_id']]['file_url_root'], $instance['det_url']));
        }
        else {
            $instance['file_url'] = $this->concat_url(array($settings[$instance['widget_id']]['file_url_root'],$settings[$instance['widget_id']]['file_url_add'], $instance['id']));
        }

        $instance['data_validity'] = $settings[$instance['widget_id']]['data_validity'];

        $data = $this->get_html_from_db($instance);

        if(count($data) < 1) {
            $data = $this->load_data($instance);
        }
        elseif($instance['data_validity'] > 0 && (time() - intval($data[0]->last_update)) > $instance['data_validity']) {
            $data = $this->load_data($instance);
        }

        $nav = $this->build_html_nav($instance['parent_code']);
        return $nav.stripslashes($data[0]->content);
    }

    private function load_data($instance) {
        $result = $this->_edg->getFile($instance);
        $instance['req_code'] = wp_remote_retrieve_response_code($result);
        $instance['req_message'] = wp_remote_retrieve_response_message($result);
        return $this->get_html_from_db($instance);
    }

    private function build_html($data, $instance) {
        $html = '<div id="eqnx-display-widget-'.$data[0]->code.'" class="eqnx-display-widget-wrapper">';
        $html .= ($instance['widget_title'] !== '') ? '<h2>'.$instance['widget_title'].'</h2>' : '';
        $html .= '<div id="wrapper-liste-'.$data[0]->code.'" class="wrapper">';
        $html .= '<input type="hidden" id="widget_id_'.$data[0]->code.'" value="'.$this->_widget_id.'" />';
        $html .= stripslashes($data[0]->content);
        $html .= '</div>';
        $html .= '<div id="wrapper-details-'.$data[0]->code.'" class="wrapper inactive">';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    private function build_html_nav($code) {
        $html = '<nav class="eqnx-nav-wrapper">';
        $html .= '<div class="eqnx-nav-slot left">';
        $html .= "<a onclick=\"displayListeWrapper('".$code."')\" class=\"nav-btn back\">Retour</a>";
        $html .= '</div>';
        $html .= '<div class="eqnx-nav-slot center">';
        $html .= '&nbsp;';
        $html .= '</div>';
        $html .= '<div class="eqnx-nav-slot right">';
        $html .= '&nbsp;';
        $html .= '</div>';
        $html .= '</nav>';

        return $html;
    }

    private function update_html($html, $nonce, $code) {
        $search = array('%%CODE%%', '%%NONCE%%');
        $replace = array($code, $nonce);
        return str_replace($search, $replace, $html);
    }

    private function getDelay($sec) {
        if($sec === '43200') {
            $result = 12;
            $suffix = ' heures';
        }
        else {
            $result = floor($sec / 3600);
            $suffix = ' heures';
            if($result > 12) {
                $result = floor($result / 24);
                $suffix = ' jours';
            }
            if($result > 7) {
                $result = floor($result / 7);
                $suffix = ' semaines';
            }
            if($result > 4) {
                $result = floor($result / 4);
                $suffix = ' mois';
            }
        }
        return $result.' '.$suffix;
    }

    private function display_error($e) {
        return '<div class="callout alert"><h5>Attention !</h5><p>'.$e.'</p></div>';
    }
}

/* Wordpress Hooks */
// add actions
add_action( 'widgets_init', create_function('', 'return register_widget("displayHtmlWidget");') );
add_action("wp_ajax_get_details", "get_details");
add_action("wp_ajax_nopriv_get_details", "get_details");

add_action( 'init', 'my_script_enqueuer' );

function my_script_enqueuer() {
    wp_register_script( "eqnx-ajax-display-html", plugins_url( 'eqnx-display-html.js', __FILE__ ), array('jquery') );
    wp_localize_script( 'eqnx-ajax-display-html', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));

    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'eqnx-ajax-display-html' );

    // enqueue styles
    wp_enqueue_style('eqnx-display-style', plugins_url( 'eqnx-display-style.css', __FILE__ ));
}

/* AJAX functions */
function get_details() {
    if ( !wp_verify_nonce( $_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $displayHtml = new displayHtmlWidget();

        $result['type'] = "success";
        $result['id'] = $_REQUEST['ident'];
        $result['parent_code'] = $_REQUEST['code'];
        $result['code'] = $_REQUEST['code'].'_'.$_REQUEST['ident'];
        $result['det_url'] = $_REQUEST['det_url'];
        $result['nonce'] = $_REQUEST['nonce'];
        $result['widget_id'] = $_REQUEST['widget_id'];
        $result['content'] = $displayHtml->get_html_content($result);

        $result = json_encode($result);
        echo $result;
    }
    else {
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }

    die();
}
?>