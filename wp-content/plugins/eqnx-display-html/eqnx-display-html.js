/**
 * Created by jclerc on 24.05.17.
 */

jQuery(document).ready( function() {

    // Récupération de détails
    jQuery('.select-details').click(function() {
        var item_id = jQuery(this).attr('data-ident');
        var item_code = jQuery(this).attr('data-code');
        var det_url = jQuery(this).attr('data-url');
        var nonce = jQuery(this).attr('data-nonce');
        var widget_id = jQuery('#widget_id_' + item_code).val();

        jQuery.ajax({
            type : 'post',
            dataType : 'json',
            url : myAjax.ajaxurl,
            data : {action: 'get_details', ident : item_id, code : item_code, det_url : det_url, nonce: nonce, widget_id: widget_id},
            success: function(response) {
                if(response.type == 'success') {
                    injectDetails(response);
                    displayDetailsWrapper(response.parent_code);
                }
                else {
                    alert('Une erreur est survenue !');
                    console.log(response);
                }
            }
        });
    });
});

function injectDetails(data) {
    var wrapper = document.getElementById('wrapper-details-' + data.parent_code);
    wrapper.innerHTML = data.content;
}

function displayDetailsWrapper(code) {
    var wrapperLst = document.getElementById('wrapper-liste-' + code);
    var wrapperDet = document.getElementById('wrapper-details-' + code);
    var wrapperWidgets = document.getElementsByClassName('eqnx-display-widget-wrapper');
    var wrapperCurrentWidget = document.getElementById('eqnx-display-widget-' + code);

    if(wrapperDet.classList.contains('inactive')) {
        wrapperLst.classList.add('inactive');
        wrapperDet.classList.remove('inactive');
        jQuery('html, body').animate({ scrollTop: 500 }, 'slow');
    }

    setTimeout(function() {
        hideMultiNodes(wrapperWidgets);
        hideNode(wrapperLst);
        showNode(wrapperCurrentWidget);
    }, 300);
}

function displayListeWrapper(code) {
    var wrapperLst = document.getElementById('wrapper-liste-' + code);
    var wrapperDet = document.getElementById('wrapper-details-' + code);
    var wrapperWidgets = document.getElementsByClassName('eqnx-display-widget-wrapper');
    var wrapperCurrentWidget = document.getElementById('eqnx-display-widget-' + code);

    if(wrapperLst.classList.contains('inactive')) {
        wrapperDet.classList.add('inactive');
        wrapperLst.classList.remove('inactive');
    }

    setTimeout(function() {
        hideNode(wrapperCurrentWidget);
        showMultiNodes(wrapperWidgets);
        showNode(wrapperLst);
    }, 300);
}

function hideNode(node) {
    node.style.display = 'none';
}

function hideMultiNodes(nodes) {
    for(var i = 0; i < nodes.length; i++) {
        nodes[i].style.display = 'none';
    }
}

function showNode(node) {
    node.style.display = 'block';
}

function showMultiNodes(nodes) {
    for(var i = 0; i < nodes.length; i++) {
        nodes[i].style.display = 'block';
    }
}