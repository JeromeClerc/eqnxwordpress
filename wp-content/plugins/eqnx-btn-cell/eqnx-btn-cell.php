<?php
/*
Plugin Name: Eqnx - Cellule bouton
Plugin URI: http://equinoxemis.ch
Description: Création d'une cellule bouton pour site
Version: 1.0
Author: Jérôme Clerc
Author URI: http://equinoxemis.ch
License: GPL2
*/

class linkCelluleWidget extends WP_Widget {
    public function __construct() {
        $widget_ops = array('classname' => 'linkCelluleWidget', 'description' => 'Affiche une case colorée avec un titre, texte & un liens' );
        parent::__construct('linkCelluleWidget', 'Eqnx - Cellule de lien', $widget_ops);
    }

    public function form($instance) {
        $instance = wp_parse_args((array) $instance, array( 'cell_bg_color' => '',
                                                            'cell_padding_size' => '',
                                                            'cell_title' => '',
                                                            'cell_txt' => '',
                                                            'cell_link_label' => '',
                                                            'cell_link_url' => '',
                                                            'cell_link_target' => '' ));
        $cellBgColor = $instance['cell_bg_color'];
        $cellPadSize = $instance['cell_padding_size'];
        $cellTitle = $instance['cell_title'];
        $cellTxt = $instance['cell_txt'];
        $cellLinkLabel = $instance['cell_link_label'];
        $cellLinkUrl = $instance['cell_link_url'];
        $cellLinkTarget = $instance['cell_link_target'];

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('cell_bg_color'); ?>">
            Couleur de fond:
            <select class="widefat" id="<?php echo $this->get_field_id('cell_bg_color'); ?>" name="<?php echo $this->get_field_name('cell_bg_color'); ?>">
                <option value="<?php echo esc_attr($cellBgColor); ?>" selected><?php echo esc_attr($cellBgColor); ?></option>
                <option value="green-mint">Vert menthe</option>
                <option value="green-olive">Vert olive</option>
                <option value="purple">Violet</option>
                <option value="orange">Orange</option>
                <option value="gray">Gris</option>
                <option value="gray-light">Gris claire</option>
            </select></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('cell_padding_size'); ?>">
            Taille de padding:
            <select class="widefat" id="<?php echo $this->get_field_id('cell_padding_size'); ?>" name="<?php echo $this->get_field_name('cell_padding_size'); ?>">
                <option value="<?php echo esc_attr($cellPadSize); ?>" selected><?php echo esc_attr($cellPadSize); ?></option>
                <option value="padding-small">small</option>
                <option value="padding-medium">medium</option>
                <option value="padding-large">large</option>
            </select></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('cell_title'); ?>">Titre: <input class="widefat" id="<?php echo $this->get_field_id('cell_title'); ?>" name="<?php echo $this->get_field_name('cell_title'); ?>" type="text" value="<?php echo esc_attr($cellTitle); ?>" /></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('cell_txt'); ?>">Texte: <textarea class="widefat" id="<?php echo $this->get_field_id('cell_txt'); ?>" name="<?php echo $this->get_field_name('cell_txt'); ?>"><?php echo esc_attr($cellTxt); ?></textarea></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('cell_link_label'); ?>">Label: <input class="widefat" id="<?php echo $this->get_field_id('cell_link_label'); ?>" name="<?php echo $this->get_field_name('cell_link_label'); ?>" type="text" value="<?php echo esc_attr($cellLinkLabel); ?>" /></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('cell_link_url'); ?>">URL: <input class="widefat" id="<?php echo $this->get_field_id('cell_link_url'); ?>" name="<?php echo $this->get_field_name('cell_link_url'); ?>" type="text" value="<?php echo esc_attr($cellLinkUrl); ?>" /></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('cell_link_target'); ?>">
            Target:
            <select class="widefat" id="<?php echo $this->get_field_id('cell_link_target'); ?>" name="<?php echo $this->get_field_name('cell_link_target'); ?>">
                <option value="<?php echo esc_attr($cellLinkTarget); ?>" selected><?php echo esc_attr($cellLinkTarget); ?></option>
                <option value="_blank">_blank</option>
                <option value="_self">_self</option>
            </select></label>
        </p>
        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['cell_bg_color'] = $new_instance['cell_bg_color'];
        $instance['cell_padding_size'] = $new_instance['cell_padding_size'];
        $instance['cell_title'] = $new_instance['cell_title'];
        $instance['cell_txt'] = $new_instance['cell_txt'];
        $instance['cell_link_label'] = $new_instance['cell_link_label'];
        $instance['cell_link_url'] = $new_instance['cell_link_url'];
        $instance['cell_link_target'] = $new_instance['cell_link_target'];
        return $instance;
    }

    public function widget($args, $instance) {
        extract($args, EXTR_SKIP);

        $html = '';

        $cellColor = empty($instance['cell_bg_color']) ? '' : $instance['cell_bg_color'];
        $cellPadSize = empty($instance['cell_padding_size']) ? 'padding-small' : $instance['cell_padding_size'];
        $cellTitle = empty($instance['cell_title']) ? '' : $instance['cell_title'];
        $cellTxt = empty($instance['cell_txt']) ? '' : $instance['cell_txt'];
        $cellLinkLabel = empty($instance['cell_link_label']) ? '' : $instance['cell_link_label'];
        $cellLinkUrl = empty($instance['cell_link_url']) ? '#' : $instance['cell_link_url'];
        $cellLinkTarget = empty($instance['cell_link_target']) ? '_self' : $instance['cell_link_target'];

        // WIDGET CODE GOES HERE

        $html .= '<div class="btn-cell-wrapper '.$cellColor.' '.$cellPadSize.'">';
        if($cellTitle !== '') {
            $html .= '<div class="row"><div class="columns"><h1>'.$cellTitle.'</h1></div></div>';
        }
        if($cellTxt !== '') {
            $html .= '<div class="row"><div class="columns">'.$cellTxt.'</div></div>';
        }
        if($cellLinkLabel !== '') {
            $html .= '<div class="row"><div class="columns"><a href="'.$cellLinkUrl.'" target="'.$cellLinkTarget.'" class="button" >'.$cellLinkLabel.'</a></div></div>';
        }
        $html .= '</div>';

        echo $html;
    }
}

add_action( 'widgets_init', create_function('', 'return register_widget("linkCelluleWidget");') );?>