<?php
/**
 * Created by PhpStorm.
 * User: jclerc
 * Date: 18.04.17
 * Time: 15:54
 */

function get_templates_lst() {
    $html = '';
    $html .= '<option value="lst_etu">Liste d\'étudiants</option>';
    $html .= '<option value="lst_form_continue">Liste de formations continues</option>';
    $html .= '<option value="lst_perfectionnements">Liste des perfectionnements</option>';
    echo $html;
}

function display_the_template($data) {
    if($data['display_template'] === 'lst_etu') {
        lst_etu($data);
    }
    if($data['display_template'] === 'lst_form_continue') {
        lst_form_continue($data);
    }
    if($data['display_template'] === 'lst_perfectionnements') {
//        lst_perfectionnements($data);
        lst_links_simple($data);
    }
    if($data['display_template'] === 'det_perfectionnements') {
        det_perfectionnements($data);
    }
}

function lst_etu($data) {
    $html = '<div class="lst-etu"><ul>';
    foreach ($data['content'] as $k => $elem) {
        $html .= '<li class="row">';
        $html .= '<div class="columns">'.$elem->etudiant.'</div>';
        $html .= '<div class="columns shrink">'.$elem->naissance.'</div>';
        $html .= '<div class="columns shrink">';
        $html .= ($elem->sexe === 'male') ? 'M' : 'F';
        $html .= '</div>';
        $html .= '<div class="columns">'.$elem->profession.'</div>';
        $html .= '</liv>';
    }
    $html .= '</ul></div>';

    echo $html;
}

function lst_form_continue($data) {
//    var_dump($data);
    $html = '<div class="lst-of-links lst-formation-continue">';
    foreach ($data['content']->plan as $plans) {
//        var_dump($elem->blocs);
        foreach ($plans as $elem) {
//            var_dump($elem->libelle);
            $html .= '<h2>'.$elem->libelle.'</h2>';
            foreach ($elem->blocs as $item) {
                $html .= '<strong>'.$item->libelle.'</strong>';
                $html .= '<ul>';
                foreach ($item->blocs as $cours) {
                    // $html .= $cours->libelle.'<br>';
                    $html .= '<li class="row">';
                    $html .= '<div class="columns">';
//                    $html .= '<a href="'.esc_url( home_url( '/' ) ).'formations-details/'.$cours->id.'" target="_self" data-id-code="'.$cours->code.'" data-id-formation="'.$cours->id.'">'.$cours->libelle.'</a>';
                    $html .= '<a href="#" target="_self" data-id-code="'.$cours->code.'" data-id-formation="'.$cours->id.'">'.$cours->libelle.'</a>';
                    $html .= '</div>';
                    $html .= '</liv>';
                }
                $html .= '</ul>';
            }
        }
        // http://localhost:8888/Wordpress/wordpress/formations-details/
    }
    $html .= '</div>';

    echo $html;
}

function lst_perfectionnements($data) {
    $html = '<div class="lst-of-links lst-perfectionnements"><ul>';
    foreach ($data['content']->plans as $k => $elem) {
        $html .= '<li class="row">';
        $html .= '<div class="columns">';
        $html .= '<a href="'.esc_url(home_url('/formations-details?code='.$data['code'].'&form-id='.$elem->id.'&rest-url='.build_rest_url($data['url'], $elem->detUrl)).'&template='.str_replace('lst', 'det', $data['template']).'&type-data='.$data['type-data']).'" target="_self">'.$elem->label.'</a>';
        $html .= '</div>';
        $html .= '</liv>';
    }
    $html .= '</ul></div>';

    echo $html;
}

function lst_links_simple($data) {
    var_dump($data);
//    echo 'Page : '.$data['details_page'].'<br>';
    $html = '<div class="lst-of-links"><ul>';
    foreach ($data['content']->plans as $k => $elem) {
        $html .= '<li class="row">';
        $html .= '<div class="columns">';
        $html .= '<a href="'.esc_url(home_url('/'.$data['details_page'].'?id='.$elem->id.'&code='.$data['code'])).'" target="_self">'.$elem->label.'</a>';
        $html .= '</div>';
        $html .= '</liv>';
    }
    $html .= '</ul></div>';
    echo $html;
}

function det_perfectionnements($data) {
    $details = $data['content']->plan->blocs[0];
    $html = '<div class="row">';
        $html .= '<div class="small-12 columns">';
            $html .= '<h1>Détails</h1>';
            $html .= '<h2>'.$details->libelle.'</h2>';
        $html .= '</div>';
    $html .= '</div>';
    foreach($details as $k => $elem) {
        if(strtolower($k) !== 'id' && strtolower($k) !== 'libelle') {
            $html .= '<div class="row">';
            $html .= '<div class="small-12 medium-5 large-4 columns">';
            $html .= '<strong>'.ucfirst(str_replace('_', ' ', $k)).'</strong>';
            $html .= '</div>';
            $html .= '<div class="small-12 medium-7 large-8 columns">';
            $html .= $elem;
            $html .= '</div>';
            $html .= '</div>';
        }
    }

    echo $html;
}

function build_rest_url($url, $restUrl) {
    if(substr($url, -1) !== '/') {
        $url = $url.'/';
    }
    if(substr($restUrl, -1) !== '/') {
        $restUrl = $restUrl.'/';
    }
    return $url.$restUrl;
}