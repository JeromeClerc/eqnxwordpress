<?php
/*
Plugin Name: Eqnx - Display Datas
Plugin URI: http://equinoxemis.ch
Description: Widget Displays - Affiche les informations reçuent de fichiers externes
Version: 1.0
Author: Jérôme Clerc
Author URI: http://equinoxemis.ch
License: GPL2
*/

class displayDatasWidget extends WP_Widget {
    private $_edg;

    public function __construct() {
        $widget_ops = array('classname' => 'displayDatasWidget', 'description' => 'Affiche les informations reçuent de fichiers externes' );
        $this->WP_Widget('displayDatasWidget', 'Eqnx - Display Datas', $widget_ops);
        $this->_edg = $GLOBALS['eqnx_edg'];
    }

    public function form($instance) {
        $instance = wp_parse_args((array) $instance, array( 'file_url' => '', 'data_format' => '', 'data_validity' => '', 'details_page' => '', 'display_template' => '' ));
        $fileUrl = $instance['file_url'];
        $dataFormat = $instance['data_format'];
        $dataValidity = $instance['data_validity'];
        $pageDetails = $instance['details_page'];
        $displayTemplate = $instance['display_template'];

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('file_url'); ?>">URL: <input class="widefat" id="<?php echo $this->get_field_id('file_url'); ?>" name="<?php echo $this->get_field_name('file_url'); ?>" type="text" value="<?php echo esc_attr($fileUrl); ?>" /></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('data_format'); ?>">
                Format des données:
                <select class="widefat" id="<?php echo $this->get_field_id('data_format'); ?>" name="<?php echo $this->get_field_name('data_format'); ?>">
                    <option value="<?php echo esc_attr($dataFormat); ?>" selected><?php echo esc_attr($dataFormat); ?></option>
                    <option value="JSON">JSON</option>
                    <option value="XML">XML</option>
                    <option value="HTML">HTML</option>
                </select></label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('data_validity'); ?>">
                Validité des données:
                <select class="widefat" id="<?php echo $this->get_field_id('data_validity'); ?>" name="<?php echo $this->get_field_name('data_validity'); ?>">
                    <option value="<?php echo esc_attr($dataValidity); ?>" selected><?php echo esc_attr($dataValidity); ?></option>
                    <option value="43200">12 heures</option>
                    <option value="86400">1 jours</option>
                    <option value="172800">2 jours</option>
                    <option value="259200">3 jours</option>
                    <option value="345600">4 jours</option>
                    <option value="432000">5 jours</option>
                    <option value="604800">1 semaine</option>
                    <option value="1209600">2 semaine</option>
                    <option value="1814400">3 semaine</option>
                    <option value="2419200">1 mois</option>
                </select>
            </label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('details_page'); ?>">
                Page affichant les détails:
                <select class="widefat" id="<?php echo $this->get_field_id('details_page'); ?>" name="<?php echo $this->get_field_name('details_page'); ?>">
                    <option value="<?php echo esc_attr($pageDetails); ?>" selected><?php echo esc_attr($pageDetails); ?></option>
                    <?php foreach(get_pages() as $k => $page) { ?>
                        <option value="<?php echo $page->post_name; ?>"><?php echo $page->post_title; ?></option>
                    <?php } ?>
                </select>
            </label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('display_template'); ?>">
                Template d'affichage:
                <select class="widefat" id="<?php echo $this->get_field_id('display_template'); ?>" name="<?php echo $this->get_field_name('display_template'); ?>">
                    <option value="<?php echo esc_attr($displayTemplate); ?>" selected><?php echo esc_attr($displayTemplate); ?></option>
                    <?php get_templates_lst(); ?>
                </select>
            </label>
        </p>
        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['file_url'] = $new_instance['file_url'];
        $instance['data_format'] = $new_instance['data_format'];
        $instance['data_validity'] = $new_instance['data_validity'];
        $instance['details_page'] = $new_instance['details_page'];
        $instance['display_template'] = $new_instance['display_template'];

        $result = $this->_edg->getFile($instance);
        $instance['req_code'] = wp_remote_retrieve_response_code($result);
        $instance['req_message'] = wp_remote_retrieve_response_message($result);

        return $instance;
    }

    public function widget($args, $instance) {
        extract($args, EXTR_SKIP);

        $fileUrl = empty($instance['file_url']) ? '' : $instance['file_url'];
        $dataFormat = empty($instance['data_format']) ? 'HTML' : $instance['data_format'];
        $displayTemplate = empty($instance['display_template']) ? '' : $instance['display_template'];

        echo '<p>****************************************************</p>';

        var_dump($instance);
//        print_r(get_pages());
//        foreach(get_pages() as $k => $page) {
//            var_dump($page);
//            echo $page->post_title.'<br>';
//        }

        echo '<p>****************************************************</p>';

        // WIDGET CODE GOES HERE
        if($instance['req_code'] === 200) {
            if ($fileUrl !== '') {
                try {
                    $data = $this->_edg->get_content(array('field' => 'url', 'value' => $fileUrl));
                    if ($data !== false && $displayTemplate !== '') {
                        if ($dataFormat !== 'HTML') {
//                            display_the_template(array('template' => $displayTemplate, 'content' => $this->convert_data($data[0]->content, $dataFormat), 'code' => $data[0]->code, 'url' => $fileUrl, 'type-data' => $instance['data_format'], 'details_page' => $instance['details_page']));
                            $instance['content'] = $this->convert_data($data[0]->content, $instance['data_format']);
                            $instance['code'] = $data[0]->code;
                            display_the_template($instance);
                        }
                        else {
                            echo $data[0]->content;
                        }
                    }
                    else {
                        echo $this->display_error('Problème avec le fichier sources...');
                    }
                }
                catch (Exception $e) {
                    echo $this->display_error($e);
                }
            }
        }
        else {
            echo $this->display_error('Erreur avec le fichier [CODE:'.$instance['req_code'].'] : '.$instance['req_message']);
        }
    }

    public function display_data($instance) {
//        var_dump($instance);

        if($instance['req_code'] === 200) {
            try {
                $data = $this->_edg->get_content(array('field' => 'url', 'value' => $instance['file_url']));
//                if(time() - $data->last_update > $instance['data_validity']) {
//                    // If data validity is overpassed, get new data from de remote file
//                    $data = $result = $this->_edg->getFile($instance);
//                }
                if ($instance['data_format'] !== 'HTML') {
                    display_the_template(array(
                        'template' => $instance['display_template'],
                        'page' => $instance['details_page'],
                        'content' => $this->convert_data($data[0]->content, $instance['type-data']),
                        'code' => $data[0]->code,
                        'url' => $instance['file_url']
                    ));
                }
                else {
                    echo $data[0]->content;
                }
            }
            catch (Exception $e) {
                echo $this->display_error($e);
            }
        }
        else {
            echo $this->display_error('Erreur avec le fichier [CODE:'.$instance['req_code'].'] : '.$instance['req_message']);
        }
    }

    private function convert_data($data, $format) {
        switch($format) {
            case 'JSON':
                $result = json_decode($data);
                break;
            case 'XML':
                $result = new SimpleXMLElement($data);
                break;
            case 'HTML':
                $result = $data;
                break;
        }
        return $result;
    }

    private function display_error($e) {
        return '<div class="callout alert"><h5>Attention !</h5><p>'.$e.'</p></div>';
    }
}

/* Include des templates */
include_once('eqnx-display-templates.php');

add_action( 'widgets_init', create_function('', 'return register_widget("displayDatasWidget");') );

/*
URL de test
https://edctest.fve.ch/isa/service/formationcourte/plan
https://edctest.fve.ch/isa/service/formationcourte/plan/matieres/833597
https://edctest.fve.ch/isa/service/perfectionnement/plans
https://edctest.fve.ch/isa/service/perfectionnement/plans/plan/823653/
https://edctest.fve.ch/isa/service/perfectionnement/plans/plan/901796/
 */
?>