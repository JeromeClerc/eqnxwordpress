/**
 * Created by jclerc on 14.06.17.
 */
jQuery(document.body).trigger('post-load');

jQuery(document).ready( function() {
    // Vider le cache
    jQuery('#trigger-empty-cache').click(function() {
        jQuery.ajax({
            type : 'post',
            dataType : 'html',
            url : myAjax.ajaxurl,
            data : {action: 'clear_cache'},
            success: function(response) {
                jQuery('#the-list').html(response);
            }
        });
    });

    // Supprimer une entrée
    jQuery('.trigger-del-item').click(function() {
        var item_id = jQuery(this).attr('data-id-item');
        var nonce = jQuery(this).attr('data-nonce');
        //console.log(window.location);

        jQuery.ajax({
            type : 'post',
            dataType : 'html',
            url : myAjax.ajaxurl,
            data : {action: 'del_element', ident : item_id, nonce: nonce },
            success: function(response) {
                if(response.length > 10) {
                    jQuery('#the-list').html(response);
                    window.location.reload();
                }
            }
        });
    });
});