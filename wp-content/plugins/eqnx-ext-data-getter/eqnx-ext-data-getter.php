<?php
/*
Plugin Name: Eqnx - External Data Getter
Plugin URI: http://equinoxemis.ch
Description: Récupération de données stockées dans des fichiers hors serveur
Version: 1.1.1
Author: Jérôme Clerc
Author URI: http://equinoxemis.ch
License: GPL2
*/

class extDataGetter {
    private
        $egd_table,
        $wpdb;

    public function __construct() {
        global $wpdb;
        $this->wpdb = &$wpdb;
        $this->egd_table = 'eqnx_external_datas';
    }

    public function getFile($instance) {
        $args = $this->get_safe_remote_args($instance['api_key']);
        // $content = wp_safe_remote_get($instance['file_url'], $args);
        $content = wp_remote_request($instance['file_url'], $args);
        if(!is_wp_error($content) && wp_remote_retrieve_response_code($content) === 200) {
            if(isset($instance['code'])) {
                $this->store_content($instance['file_url'], utf8_encode($content['body']), $instance['code']);
            }
            else {
                $this->store_content($instance['file_url'], utf8_encode($content['body']));
            }
        }
        return $content;
    }

    private function store_content($url, $content, $code='') {
        if($code === '') {
            $code = $this->get_id_code();
        }

        if(!$this->test_one_field(array('field' => 'url', 'value' => $url))) {
            if(!$this->insert_content($url, $code, $content)) {
                $this->store_content($url, '', $content);
            }
        }
        else {
            $this->update_content($url, $content);
        }
    }

    public function get_content($data) {
        return $this->wpdb->get_results("SELECT * FROM ".$this->egd_table." WHERE ".$data['field']." = '".$data['value']."'");
    }

    public function get_all_items($sort='id', $order='ASC') {
        return $this->wpdb->get_results("SELECT * FROM ".$this->egd_table." ORDER BY ".$sort."  ".$order);
    }

    public function insert_content($url, $code, $content) {
        $lastUpdate = time();
        $slashed_content = addslashes($content);
        return $this->wpdb->insert($this->egd_table, array('url' => $url, 'code' => $code, 'content' => $slashed_content, 'last_update' => $lastUpdate));
    }

    public function update_content($url, $content) {
        $lastUpdate = time();
        return $this->wpdb->update($this->egd_table, array('content' => $content, 'last_update' => $lastUpdate), array('url' => $url));
    }

    public function delete_content($data) {
        return $this->wpdb->delete($this->egd_table, $data);
    }

    public function clear_cache() {
        return $this->wpdb->query($this->wpdb->prepare("TRUNCATE ".$this->egd_table));
    }

    private function test_one_field($data) {
        if(count($this->wpdb->get_results("SELECT * FROM ".$this->egd_table." WHERE ".$data['field']." = '".$data['value']."'")) > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    private function get_safe_remote_args($key) {
        if($key != '') {
            return array('headers' => array('Authorization' => $this->get_auth_type($key).' ' . $key));
        }
        else {
            return array();
        }
    }

    private function get_auth_type($key) {
        if(!stripos($key, ':')) {

            return 'APIKey';
        }
        else {
            return 'Basic';
        }
    }

    public function get_id_code() {
        return uniqid(time().'_eqnx_');
    }

    public static function eqnx_edg_install() {
        global $wpdb;
        $table_name = 'eqnx_external_datas';
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$table_name} (
                      `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
                      `url` TEXT NOT NULL ,
                      `code` VARCHAR(255) NOT NULL ,
                      `content` LONGTEXT NOT NULL ,
                      `last_update` VARCHAR(255) NOT NULL ,
                      PRIMARY KEY (`id`) ,
                      UNIQUE (code))");
    }

    public static function eqnx_edg_uninstall() {
        global $wpdb;
        $table_name = 'eqnx_external_datas';
        $wpdb->query("DROP TABLE IF EXISTS {$table_name}");
    }
}
$GLOBALS['eqnx_edg'] = new extDataGetter();

// ----- Ajout des scripts pour utiliser l'AJAX de Wordpress -----
add_action( 'wp_ajax_clear_cache', 'clear_cache' );
add_action( 'wp_ajax_nopriv_clear_cache', 'clear_cache' );
add_action( 'wp_ajax_del_element', 'del_element' );
add_action( 'wp_ajax_nopriv_del_element', 'del_element' );

add_action("wp_ajax_clear_cache", "clear_cache");

add_action( 'admin_enqueue_scripts', 'my_enqueue' );

function my_enqueue() {
    wp_enqueue_script( "ajax-script", plugins_url( 'eqnx-ext-data-getter.js', __FILE__ ), array('jquery') );

    // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
    wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' )));
}

// ----- Ajouter un menu dans l'admin de Wordpress -----
add_action('admin_menu', 'external_data_getter_setup_menu');

function external_data_getter_setup_menu() {
    add_menu_page( 'Gestion des données externes', 'Données externes', 'manage_options', 'external-data-getter', 'setup_page_init' );
}

function setup_page_init() {
    $html  = '<div class="wrap">';
    $html .= '<h1 class="wp-heading-inline">External Data Getter</h1>';
    $html .= '<form class="posts-filter" method="get">';
    $html .= '<div class="tablenav top">';
    $html .= '<div class="alignleft actions bulkactions">';
    $html .= '<input type="button" id="trigger-empty-cache" class="button action" value="Tout supprimer"  />';
    $html .= '<br class="clear">';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '<table class="wp-list-table widefat fixed striped posts">';
    $html .= '<thead><tr>';
    $html .= '<th scope="col" id="url" class="manage-column column-url column-primary sortable desc">';
    $html .= '<a href="#"><span>URL</span></a>';
    $html .= '</th>';
    $html .= '<th scope="col" id="update" class="manage-column column-update column-primary sortable desc">';
    $html .= '<a href="#"><span>Dernière mise à jour</span></a>';
    $html .= '</th>';
    $html .= '<th>&nbsp;</th>';
    $html .= '</tr></thead>';
    $html .= '<tbody id="the-list">';
    $html .= get_list_item_in_cache();
    $html .= '</tbody>';
    $html .= '<tfoot>';
    $html .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
    $html .= '</tfoot>';
    $html .= '</table>';
    $html .= '</form>';
    $html .= '</div>';

    echo $html;
}

function get_list_item_in_cache() {
    $edg  = new extDataGetter();
    $list = $edg->get_all_items('url');
    $nonce = wp_create_nonce("my_user_vote_nonce");

    $html = '';
    foreach($list as $k => $item) {
        $html .= '<tr>';
        $html .= '<td><a href="'.$item->url.'" target="_blank">'.$item->url.'</a></td>';
        $html .= '<td>'.date('d.m.Y à H:m:s', $item->last_update).'</td>';
        $html .= '<td><input type="button" class="button action trigger-del-item" value="Supprimer" data-id-item="'.$item->id.'" data-nonce="'.$nonce.'" /></td>';
        $html .= '</tr>';
    }

    return $html;
}

// ----- AJAX functions -----
function clear_cache() {
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $edg  = new extDataGetter();
        $edg->clear_cache();
        echo '<tr><td>Aucune données en cache...</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
    }
    else {
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }

    wp_die();
}

function del_element() {
    if ( !wp_verify_nonce( $_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $edg  = new extDataGetter();
        $result = $edg->delete_content(array('id' => $_REQUEST['ident']));
        if($result > 0) {
            echo get_list_item_in_cache();
        }
    }
    else {
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }

    wp_die();
}

// ----- Registration sur plugin -----
register_activation_hook(__FILE__, array('extDataGetter', 'eqnx_edg_install'));
register_uninstall_hook(__FILE__, array('extDataGetter', 'eqnx_edg_uninstall'));
?>