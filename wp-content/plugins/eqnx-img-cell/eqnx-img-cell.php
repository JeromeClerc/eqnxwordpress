<?php
/*
Plugin Name: Eqnx - Cellule image
Plugin URI: http://equinoxemis.ch
Description: Création d'une cellule qui affiche une image
Version: 1.0
Author: Jérôme Clerc
Author URI: http://equinoxemis.ch
License: GPL2
*/

class imgCelluleWidget extends WP_Widget {
    public function __construct() {
        $widget_ops = array('classname' => 'imgCelluleWidget', 'description' => 'Affiche une image' );
        parent::__construct('imgCelluleWidget', 'Eqnx - Cellule image', $widget_ops);
    }

    public function form($instance) {
        $instance = wp_parse_args((array) $instance, array( 'img_name' => '', 'img_url' => '' ));
        $imgName = $instance['img_name'];
        $imgUrl = $instance['img_url'];

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('img_url'); ?>">
            Image de fond:
            <select class="widefat" id="<?php echo $this->get_field_id('img_url'); ?>" name="<?php echo $this->get_field_name('img_url'); ?>">
                <option value="<?php echo esc_attr($imgUrl); ?>" selected><?php echo esc_attr($imgName); ?></option>
                <?php echo $this->get_media_lst(); ?>
            </select></label>

        </p>
        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['img_name'] = $new_instance['img_name'];
        $instance['img_url'] = $new_instance['img_url'];
        return $instance;
    }

    public function widget($args, $instance) {
        extract($args, EXTR_SKIP);

        $html = '';
        $imgUrl = empty($instance['img_url']) ? '#' : $this->rebuildUrl($instance['img_url']);

        // WIDGET CODE GOES HERE
        $html .= '<div class="img-cell-wrapper" style="background-image: url(\''.$imgUrl.'\');">';
        $html .= '&nbsp;';
        $html .= '</div>';

        echo $html;
    }

    public function rebuildUrl($url) {
        $segments  = explode('/', $url);
        $result = 'http://';
        foreach($segments as $k => $segment) {
            if($k > 1) {
                $result .= $segment;

                if($k < (count($segments)-1)) {
                    $result .= '/';
                }
            }
        }
        return $result;
    }

    public function get_media_lst() {
        $query_images_args = array(
            'post_type'      => 'attachment',
            'post_mime_type' => 'image',
            'post_status'    => 'inherit',
            'posts_per_page' => - 1,
        );

        $query_images = new WP_Query($query_images_args);

        $images = array();
        $html = '';
        foreach ( $query_images->posts as $image ) {
            $html .= '<option value="'.$image->guid.'">'.$image->post_title.'</option>';
        }
        return $html;
    }
}

add_action( 'widgets_init', create_function('', 'return register_widget("imgCelluleWidget");') );?>